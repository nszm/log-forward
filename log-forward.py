#!/usr/bin/env python3

# NSzM Software Laboratories

from smtplib import SMTP_SSL
import datetime
import ssl
import sys
import os
from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer
from functools import partial
import logging
import traceback
import argparse
import time

HOST      = 'smtp.MAILHOST.com'
PORT      = 465 # or something else
FROM_ADDR = # SENDER@DOMAIN.COM
TO_ADDR   = # RECIPIENT@DOMAIN.COM
USER      = # MAILUSER, probably must be FROM_ADDR
PASSWORD  = # MAILPASS
LOG_LEVEL = logging.INFO

def sendmail(from_addr, to_addr, subj, message_text):
    date = datetime.datetime.now().strftime("%d/%m/%Y %H:%M")
    msg = "From: {}\nTo: {}\nSubject: {}\nDate: {}\n\n{}".format(from_addr, to_addr, subj, date, message_text)
    
    with SMTP_SSL(HOST, PORT, context=ssl.create_default_context()) as server:
        try:
            server.login(USER, PASSWORD)
            server.sendmail(from_addr, to_addr, msg)
        except:
            logging.error(traceback.format_exc())
        else:
            logging.info("Mail sent successfully.")

def on_any_event(event, filepath, sender, recipient):
    time.sleep(1) # to ensure the file's already written...I'm not proud for this solution
    
    if os.path.isfile(event.src_path) and event.src_path == filepath:
        try:
            with open(event.src_path, 'r') as f:
                 message_text = f.read()
                 
                 # Examine the subject from the message text if possible
                 if "Subject: " in message_text:
                     start = message_text.index("Subject: ") + len("Subject: ")
                     end = message_text.index("\n", start)
                     subject = message_text[start:end]
                 else:
                     subject = "Event on server"
                 
        except:
            logging.error(traceback.format_exc())
        else:
            sendmail(sender, recipient, subject, message_text)

        try:
            os.remove(event.src_path)
        except:
            logging.error(traceback.format_exc())

def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument("watchpath", help="The path of your mail's file where \
                                           the system messages arrive \
                                           eg. /var/mail/YOURUSERNAME")
    parser.add_argument("logpath", help="Path of the log file")
    args = parser.parse_args()
    
    watchpath = os.path.abspath(args.watchpath)
    watchdir = os.path.dirname(watchpath)  
    if not os.path.isdir(watchdir):
        sys.exit("Error: '{}' directory does not exists. Exiting.".format(watchdir))
    
    f = '%(asctime)s - %(levelname)s: %(message)s'
    logging.basicConfig(filename=args.logpath, format=f, level=LOG_LEVEL)

    on_event_part = partial(on_any_event, filepath=watchpath, sender=FROM_ADDR, recipient=TO_ADDR)
    event_handler = FileSystemEventHandler()
    event_handler.on_created = on_event_part
    event_handler.on_modified = on_event_part
    
    observer = Observer()
    observer.schedule(event_handler, watchdir, recursive=False)
    
    try:
        observer.start()
        while True:
            time.sleep(1)
    finally:
        observer.stop()
        observer.join()

if __name__ == "__main__":   
    main(sys.argv)
