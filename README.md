# log forward

The purpose of this program is to easily send the locally generated mails (about problems) on your Linux server to your e-mail. On Linux the mails usually stored in a folder, for example in "/var/mail". This program watches your user's mail file (via watchdog). If the file is created (new message from the system) this program reads the message, sends it to your e-mail, then deletes the file from the filesystem. This program runs forever, watches the given directory, so you should make it to start on system bootup.
If you would like to get other user's messages, you should forward them with '/etc/aliases'.
